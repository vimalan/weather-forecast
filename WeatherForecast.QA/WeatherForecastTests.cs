using FluentAssertions;
using System.Collections.Generic;
using System.Net.Http;
using System.Text.Json;
using System.Threading.Tasks;
using Xunit;

namespace WeatherForecast.QA
{
    public class WeatherForecastTests
    {
        [Fact]
        public async Task Get_ShouldReturnWeatherForecast()
        {
            string URL = "http://localhost/weatherforecast";
            using HttpClient client = new HttpClient();

            var response = await client.GetAsync(URL);

            response.StatusCode.Should().Be(200);
            var content = await response.Content.ReadAsStringAsync();
            var weather = JsonSerializer.Deserialize<IEnumerable<Models.WeatherForecast>>(content);
            weather.Should().HaveCount(5);
        }
    }
}
