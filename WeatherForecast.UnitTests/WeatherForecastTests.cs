using AutoFixture;
using AutoFixture.AutoNSubstitute;
using AutoFixture.Xunit2;
using FluentAssertions;
using WeatherForecast.Controllers;
using Xunit;

namespace WeatherForecast.UnitTests
{
    public class WeatherForecastTests
    {
        [Theory, DefaultAutoData]
        public void Get_ShouldReturnWeatherForecast(WeatherForecastController sut)
        {
            var result = sut.Get();

            result.Should().HaveCount(5);
            result.Should().ContainItemsAssignableTo<Models.WeatherForecast>();
        }

        [Theory, DefaultAutoData]
        public void Get_TempFShouldMatchWithTempC(WeatherForecastController sut)
        {
            var result = sut.Get();

            foreach (var forecast in result)
            {
                forecast.TemperatureF.Should().Be(32 + (int)(forecast.TemperatureC / 0.5556));
            }
        }
    }

    public class DefaultAutoDataAttribute : AutoDataAttribute
    {
        public DefaultAutoDataAttribute() : base(() => new Fixture().Customize(new DefaultCustomization()))
        {

        }
    }

    public class DefaultCustomization : CompositeCustomization
    {
        public DefaultCustomization() : base(new AutoNSubstituteCustomization(), new OmitBindingInfoCustomization())
        {

        }
    }

    public class OmitBindingInfoCustomization : ICustomization
    {
        public void Customize(IFixture fixture)
        {
            //https://github.com/AutoFixture/AutoFixture/issues/1141
            fixture.Customize<Microsoft.AspNetCore.Mvc.ModelBinding.BindingInfo>(c => c.OmitAutoProperties());
        }
    }

}
